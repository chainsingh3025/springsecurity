package com.example.springsecurity.demo.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.example.springsecurity.demo.security.Role.ADMIN;
import static com.example.springsecurity.demo.security.Role.STUDENT;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;

    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()   // authorize request
            .antMatchers("/", "index", "/css/**")   //some endpoint which we don't want to authenticate as they public
            .permitAll()
            .anyRequest()   // all request must be authenticated
            .authenticated()
            .and()
            .httpBasic();  // using basic authentication
    }

    @Override
    @Bean
    protected UserDetailsService userDetailsService() {
        // in memory user
        UserDetails chain = User.builder()
            .username("chain")
            .password(passwordEncoder.encode("password"))
            .roles(STUDENT.name())  //ROLE_STUDENT
            .build();

        UserDetails sing = User.builder()
            .username("singh")
            .password(passwordEncoder.encode("password"))
            .roles(ADMIN.name())  //ROLE_STUDENT
            .build();
        return new InMemoryUserDetailsManager(chain, sing);

    }
}
