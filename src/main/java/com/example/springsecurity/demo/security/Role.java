package com.example.springsecurity.demo.security;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public enum Role {
    STUDENT(Collections.emptySet()),
    ADMIN(Set.of(UserPermission.COURSE_READ, UserPermission.COURSE_WRITE));

    private final Set<UserPermission> permission;

    Role(Set<UserPermission> permission) {
        this.permission = permission;
    }
}
