package com.example.springsecurity.demo.student;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/student")
public class StudentController {

    private static final List<Student> STUDENTS = Arrays.asList(
        new Student(1,"james"),
        new Student(2,"bond"),
        new Student(3,"tom"),
        new Student(4,"cruise")
    );
    @GetMapping(path = "/{studentId}")
    public Student getStudent(@PathVariable("studentId") Integer id) {
        return STUDENTS.stream()
            .filter(student -> id.equals(student.getId()))
            .findFirst()
            .orElseThrow(
                () -> new IllegalArgumentException("student "+id + " doesnt exists")
            );
    }
}
